#ifndef OPENVR_TO_CINDER_H
#define OPENVR_TO_CINDER_H 

#include"openvr.h"
#include"cinder/app/App.h"

namespace vr{
	inline ci::mat4 to_cinder(HmdMatrix34_t const& m){
		return ci::mat4(
			m.m[0][0], m.m[1][0], m.m[2][0], 0.0f,
			m.m[0][1], m.m[1][1], m.m[2][1], 0.0f,
			m.m[0][2], m.m[1][2], m.m[2][2], 0.0f,
			m.m[0][3], m.m[1][3], m.m[2][3], 1.0f
		);
	}

	inline ci::mat4 to_cinder(HmdMatrix44_t const& m){
		return ci::mat4(
			m.m[0][0], m.m[1][0], m.m[2][0], m.m[3][0],
			m.m[0][1], m.m[1][1], m.m[2][1], m.m[3][1], 
			m.m[0][2], m.m[1][2], m.m[2][2], m.m[3][2], 
			m.m[0][3], m.m[1][3], m.m[2][3], m.m[3][3]
		);
	}

	inline ci::vec3 to_cinder(HmdVector3_t const& v){
		return ci::vec3(v.v[0],v.v[1],v.v[2]);
	}

	inline ci::vec4 to_cinder(HmdVector4_t const& v){
		return ci::vec4(v.v[0],v.v[1],v.v[2],v.v[3]);
	}

}

#endif /* OPENVR_TO_CINDER_H */
