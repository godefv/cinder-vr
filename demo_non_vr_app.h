#pragma once

#include"cinder/app/App.h"
#include"cinder/CameraUi.h"

struct DemoApp:public ci::app::App{
	void draw() override;
	void update() override;
	void mouseDown(ci::app::MouseEvent event) override;
	void mouseDrag(ci::app::MouseEvent event) override;
	~DemoApp() override=default;

	//spectator
	ci::CameraPersp	spectator_camera;
	ci::CameraUi spectator_camera_ui;
};

