#ifndef VR_INTERFACE_H
#define VR_INTERFACE_H 

#include"openvr.h"
#if _WIN64
#include<windows.h>
#endif
#include"GL/gl.h"

#include"error_checking/return_status.h"

#include<iostream>
#include<string>
#include<array>

struct tracked_devices_pose_t{
	vr::TrackedDevicePose_t hmd;
	vr::TrackedDevicePose_t left_controller;
	vr::TrackedDevicePose_t right_controller;
};

struct vr_interface_t{
	vr::IVRSystem* openvr_system=nullptr;
	godefv::return_status_t status{"not_initialized"};
	uint32_t width,height;

	//! The constructor initializes OpenVR.
	//! In case of failure, put a meaningful error in this->status
	vr_interface_t();
	~vr_interface_t();

	//! Warning: this function should be called only once per frame.
	//! First, because it is blocking to respect the framerate setting in SteamVR.
	//! Second, because it probably predicts the positions at the time the rendering 
	//! will be submitted, instead of returning the current ones. So don't worry about 
	//! having out of date positions.
	auto get_devices_pose_and_motion_array() -> std::array<vr::TrackedDevicePose_t, vr::k_unMaxTrackedDeviceCount>;
	auto get_devices_pose_and_motion() -> tracked_devices_pose_t;

	//! Sends the undistorted rendered textures to the VR system.
	//! The VR system takes care of VR specific issues such as distortion, prediction, synchronization.
	void send_rendered_images(GLint left_texture_id, GLint right_texture_id);

	void vibrate_controller(vr::ETrackedControllerRole role, uint32_t axis, unsigned short duration);
};

std::ostream& operator<<(std::ostream& out, vr::HmdMatrix34_t const& vr_matrix);

#endif /* VR_INTERFACE_H */
