#include"cinder/gl/gl.h"
#include"demo_non_vr_app.h"

namespace gl=ci::gl;

void DemoApp::mouseDown(ci::app::MouseEvent event){
	this->spectator_camera_ui.mouseDown(event);
}

void DemoApp::mouseDrag(ci::app::MouseEvent event){
	this->spectator_camera_ui.mouseDrag(event);
}

void DemoApp::update(){
	//set camera for non vr view
	gl::setMatrices(this->spectator_camera);
}

void DemoApp::draw(){
	gl::enableDepthWrite();
	gl::enableDepthRead();
	gl::clear();
}
