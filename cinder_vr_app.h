#ifndef CINDER_VR_APP_H
#define CINDER_VR_APP_H 

#include"cinder/gl/gl.h"
#include"vr_interface.h"
#include"openvr_to_cinder.h"

#include"cinder/app/App.h"
#include"cinder/gl/Fbo.h"
#include"cinder/ImageIo.h"
#include"error_checking/exit.h"

#include<iostream>
#include<algorithm>
#include<vector>
#include<array>


namespace gl=ci::gl;

template<class NonVrApp>
struct CinderVrApp : public NonVrApp {
	template<class... T>
	CinderVrApp(T&&...);
	~CinderVrApp() override=default;
	void update() override;
	void setup() override;
	void draw() override;
	void keyDown(ci::app::KeyEvent) override;

	vr_interface_t vr_interface;
	std::array<gl::FboRef,2> eye_fbos;
	std::array<ci::mat4,2> eye_projections;
	tracked_devices_pose_t tracked_devices_pose;

	bool save_next_image=false;
};

template<class NonVrApp> template<class... T>
CinderVrApp<NonVrApp>::CinderVrApp(T&&... args):NonVrApp{std::forward<T>(args)...}
{}

template<class NonVrApp>
void CinderVrApp<NonVrApp>::setup(){
	if(!vr_interface.status.no_error){
		std::cerr<<"VR is disabled due to error: "<<vr_interface.status.msg<<std::endl;
		NonVrApp::setup();
		return;
	}
	gl::enableVerticalSync(false);
	this->disableFrameRate();
	for(auto eye:{vr::EVREye::Eye_Left, vr::EVREye::Eye_Right}){
		this->eye_fbos[eye]=gl::Fbo::create(vr_interface.width, vr_interface.height);
		this->eye_projections[eye]=to_cinder(vr_interface.openvr_system->GetProjectionMatrix(eye, get_near_distance(*this), get_far_distance(*this)));
	}
	NonVrApp::setup();
}

//default callback functions
namespace cinder::app{
	template<class NonVrApp>
	void draw_left_controller(NonVrApp const&){
		gl::color(ci::Color(1,1,1));
		gl::drawCube(ci::vec3(),ci::vec3(0.1f, 0.05f, 0.2f));
	}
	template<class NonVrApp>
	void draw_right_controller(NonVrApp const&){
		gl::color(ci::Color(0.2f,0.2f,0.2f));
		gl::drawCube(ci::vec3(),ci::vec3(0.1f, 0.05f, 0.2f));
	}
	template<class NonVrApp>
	void pre_update(NonVrApp const&){
	}
	template<class NonVrApp>
	void post_update(NonVrApp const&){
	}
	template<class NonVrApp>
	void pre_draw(NonVrApp const&){
	}
	template<class NonVrApp>
	void post_draw(NonVrApp const&){
	}
	template<class NonVrApp>
	float get_near_distance(NonVrApp const&){
		return 0.01f;
	}
	template<class NonVrApp>
	float get_far_distance(NonVrApp const&){
		return 300.f;
	}
	template<class NonVrApp>
	ci::mat4 get_vr_space_to_world_matrix(NonVrApp const&){
		return {};
	}
}

//
template<class NonVrApp>
void CinderVrApp<NonVrApp>::update(){
	if(!vr_interface.status.no_error){
		NonVrApp::update();
		return;
	}
	this->tracked_devices_pose=vr_interface.get_devices_pose_and_motion();
	pre_update(*this);
	NonVrApp::update();
	post_update(*this);
}

template<class NonVrApp>
void CinderVrApp<NonVrApp>::draw(){
	if(!vr_interface.status.no_error){
		NonVrApp::draw();
		return;
	}
	//draw non vr view
	gl::viewport(ci::vec2(this->getWindowWidth(), this->getWindowHeight()));
	NonVrApp::draw();

	//draw vr view
	if(!this->tracked_devices_pose.hmd.bDeviceIsConnected){
		std::cerr<<"Please connect your Head Mounted Display."<<std::endl;
		return;
	}
	auto head_to_vr_space_matrix=to_cinder(this->tracked_devices_pose.hmd.mDeviceToAbsoluteTracking);
	for(auto eye:{vr::EVREye::Eye_Left, vr::EVREye::Eye_Right}){
		auto eye_to_head_matrix=to_cinder(vr_interface.openvr_system->GetEyeToHeadTransform(eye));
		auto vr_space_to_world_matrix=get_vr_space_to_world_matrix(*this);
		gl::setViewMatrix(inverse(vr_space_to_world_matrix*head_to_vr_space_matrix*eye_to_head_matrix));
		gl::setProjectionMatrix(eye_projections[eye]);

		eye_fbos[eye]->bindFramebuffer();
		gl::viewport(ci::vec2(vr_interface.width, vr_interface.height));

		pre_draw(*this);
		NonVrApp::draw();
		post_draw(*this);

		//left controller
		if(this->tracked_devices_pose.left_controller.bDeviceIsConnected){
			auto controller_to_world_matrix=to_cinder(this->tracked_devices_pose.left_controller.mDeviceToAbsoluteTracking);
			gl::pushModelMatrix();
			gl::setModelMatrix(controller_to_world_matrix);
			draw_left_controller(*this);
			gl::popModelMatrix();
		}
		//right controller
		if(this->tracked_devices_pose.right_controller.bDeviceIsConnected){
			auto controller_to_world_matrix=to_cinder(this->tracked_devices_pose.right_controller.mDeviceToAbsoluteTracking);
			gl::pushModelMatrix();
			gl::setModelMatrix(controller_to_world_matrix);
			draw_right_controller(*this);
			gl::popModelMatrix();
		}

		eye_fbos[eye]->unbindFramebuffer();
	}
	vr_interface.send_rendered_images(
		eye_fbos[vr::EVREye::Eye_Left ]->getColorTexture()->getId(),
		eye_fbos[vr::EVREye::Eye_Right]->getColorTexture()->getId()
	);
	if(save_next_image){
		ci::writeImage( ci::fs::path{"/tmp/myPixels.png"}, eye_fbos[vr::EVREye::Eye_Left]->getColorTexture()->createSource() );
		std::cout<<"Saved left eye texture."<<std::endl;
		save_next_image=false;
	}
}

template<class NonVrApp>
void CinderVrApp<NonVrApp>::keyDown(ci::app::KeyEvent key){
	switch(key.getChar()){
		case 's':
		save_next_image=true;
		return;
	}
	NonVrApp::keyDown(key);
};

#endif /* CINDER_VR_APP_H */
