This allows to add VR to any non-VR Cinder App in just a few lines of code. 

First, you `#include "cinder_vr_app.h"` and replace your `CINDER_APP(YourApp, ...)` macro by `CINDER_APP(CinderVrApp<YourApp>, ...)`.

Then, you implement the callbacks you need, such as :
```
float get_far_distance(YourApp const& app){}
ci::mat4 get_vr_space_to_world_matrix(YourApp const& app){}
```

The full list of available callbacks can be found in [cinder_vr_app.h](cinder_vr_app.h#L59).
Implementing these callbacks is not allways mandatory but provides the flexibility to work with various Cinder apps. I can add more if needed.

# Supported backends

Only OpenVR is currently supported, but it is accessed exclusively through [vr_interface_t](vr_interface.h), so more backends could be added by implementing the same interface.

# Install runtime dependencies

* SteamVR : Install Steam. Then install SteamVR from it.

# Install compilation tools

* cmake
* git

On Linux :
* gcc 

On Windows
* clang (https://github.com/llvm/llvm-project/releases/download/llvmorg-11.0.0/LLVM-11.0.0-win64.exe)
* Visual Studio 2019 (https://visualstudio.microsoft.com/fr/downloads/)

On Windows, clang links by default to Visual Studio libraries, so you need Visual Studio as well.
As Visual Studio 2019 does not support all required C++20 features, you still need to compile with Clang. GCC with Mingw should also work but has not been tested.

# Install build dependencies

## Short story
Just download the submodules :
```
git submodule update --init --recursive
```

## Details

* OpenGL : It is probably already installed on your computer. CMake will probably find it.
* Cinder : `git submodule update --init --recursive src/extern/cinder`
* godefv::error_checking : `git submodule update --init --recursive src/extern/error_checking`
* OpenVR : You can get the submodule `git submodule update --init --recursive src/extern/openvr`, but it is very big because it includes compiled binaries for all releases. To reduce the amount of download, you can install it through the package manager of your OS, or download and unzip the latest release at https://github.com/ValveSoftware/openvr/releases .


# Build

```
cmake -H. -B_build -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DOPENVR_ROOT_DIR=extern/openvr
cmake --build _build
```
If you have not got OpenVR through the git submodule, change OPENVR_ROOT_DIR to your OpenVR install path, or just omit it if you have installed OpenVR through your package manager on Linux.
