#include"vr_interface.h"

#include<algorithm>
#include<iostream>
#include<vector>

std::string get_tracked_device_property_string(vr::IVRSystem* openvr_system, vr::TrackedDeviceIndex_t unDevice, vr::TrackedDeviceProperty prop, vr::TrackedPropertyError *peError=NULL){
	size_t result_length = openvr_system->GetStringTrackedDeviceProperty( unDevice, prop, NULL, 0, peError );
	std::string result(result_length, ' '); 
	openvr_system->GetStringTrackedDeviceProperty( unDevice, prop, result.data(), result.size(), peError );
	return result;
}

std::vector<vr::TrackedDeviceIndex_t> get_tracked_device_indices(vr::IVRSystem* openvr_system, vr::ETrackedDeviceClass device_class){
	uint32_t result_length = openvr_system->GetSortedTrackedDeviceIndicesOfClass(device_class, nullptr, 0);
	std::vector<vr::TrackedDeviceIndex_t> result{result_length};
	openvr_system->GetSortedTrackedDeviceIndicesOfClass(device_class, result.data(), result.size());
	return result;
}

vr_interface_t::vr_interface_t(){
	this->status=[this](){
		if(!vr::VR_IsHmdPresent())           return godefv::return_status_t{"HMD not detected on the system"};
		if(!vr::VR_IsRuntimeInstalled())     return godefv::return_status_t{"OpenVR Runtime not detected on the system"};

		vr::EVRInitError vr_error = vr::VRInitError_None;
		this->openvr_system = vr::VR_Init(&vr_error, vr::VRApplication_Scene);
		if(vr_error != vr::VRInitError_None) return godefv::return_status_t{std::string{"Unable to initialize OpenVR ("}+vr::VR_GetVRInitErrorAsEnglishDescription(vr_error)+")"};

		std::clog<<get_tracked_device_property_string(openvr_system, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_TrackingSystemName_String)<<std::endl;
		std::clog<<get_tracked_device_property_string(openvr_system, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_SerialNumber_String)<<std::endl;

		if(!vr::VRCompositor())              return godefv::return_status_t{"Unable to initialize OpenVR compositor"};

		openvr_system->GetRecommendedRenderTargetSize(&this->width, &this->height);
		std::clog<<"Initialized HMD with suggested render target size : "<<width<<"x"<<height<< std::endl;

		return godefv::return_status_t{};
	}();
}

vr_interface_t::~vr_interface_t(){
	vr::VR_Shutdown();
}

auto vr_interface_t::get_devices_pose_and_motion_array() -> std::array<vr::TrackedDevicePose_t, vr::k_unMaxTrackedDeviceCount>{
	std::array<vr::TrackedDevicePose_t, vr::k_unMaxTrackedDeviceCount> tracked_devices_poses;
	auto error_status=vr::VRCompositor()->WaitGetPoses(tracked_devices_poses.data(), tracked_devices_poses.size(), nullptr, 0);
	if(error_status==vr::VRCompositorError_DoNotHaveFocus) std::clog<<"WaitGetPoses() returned DoNotHaveFocus"<<std::endl;
	return tracked_devices_poses;
}

auto vr_interface_t::get_devices_pose_and_motion() -> tracked_devices_pose_t{
	auto devices_pose_and_motion=this->get_devices_pose_and_motion_array();
	auto get_controller_pose=[this,&devices_pose_and_motion](vr::ETrackedControllerRole role){
		auto index=this->openvr_system->GetTrackedDeviceIndexForControllerRole(role);
		if(index<devices_pose_and_motion.size()){
			return devices_pose_and_motion[index];
		}else{
			vr::TrackedDevicePose_t pose_not_found;
			pose_not_found.bDeviceIsConnected=false;
			pose_not_found.bPoseIsValid=false;
			return pose_not_found;
		}
	};
	return tracked_devices_pose_t{
		devices_pose_and_motion[vr::k_unTrackedDeviceIndex_Hmd],
		get_controller_pose(vr::ETrackedControllerRole::TrackedControllerRole_LeftHand),
		get_controller_pose(vr::ETrackedControllerRole::TrackedControllerRole_RightHand),
	};
}

void vr_interface_t::send_rendered_images(GLint left_texture_id, GLint right_texture_id){
	// vr::Texture_t is a C-style variant. The agrument vr::TextureType_OpenGL, tells it which type the texture id is.
	vr::Texture_t const left_vr_texture ={reinterpret_cast<void*>(static_cast<ptrdiff_t>(left_texture_id )), vr::TextureType_OpenGL, vr::ColorSpace_Auto};
	vr::Texture_t const right_vr_texture={reinterpret_cast<void*>(static_cast<ptrdiff_t>(right_texture_id)), vr::TextureType_OpenGL, vr::ColorSpace_Auto};
	vr::VRCompositor()->Submit(vr::Eye_Left , &left_vr_texture);
	vr::VRCompositor()->Submit(vr::Eye_Right, &right_vr_texture);

	glFlush();
	glFinish();
	vr::VRCompositor()->PostPresentHandoff();
}

void vr_interface_t::vibrate_controller(vr::ETrackedControllerRole role, uint32_t axis, unsigned short duration){
	auto index=this->openvr_system->GetTrackedDeviceIndexForControllerRole(role);
	this->openvr_system->TriggerHapticPulse(index, axis, duration);
}

std::ostream& operator<<(std::ostream& out, vr::HmdMatrix34_t const& vr_matrix){
	for(auto row:vr_matrix.m){
		for(int i=0; i<4; ++i){
			out<<row[i]<<" ";
		}
		out<<std::endl;
	}
	return out;
}

